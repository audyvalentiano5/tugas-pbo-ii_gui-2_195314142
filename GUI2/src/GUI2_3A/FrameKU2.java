package GUI2_3A;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
public class FrameKU2 extends JFrame {
    public FrameKU2(){
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class Turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel = new JPanel();
        JTextField text = new JTextField(null);
        text.setBounds(18, 41, 251, 21);
        panel.add(text);
        this.add(panel);
        
        JCheckBox box = new JCheckBox("box");
        box.setBounds(119, 13, 64, 21);
        panel.add(box);
        this.add(panel);
        
        JRadioButton radio = new JRadioButton("radio");
        radio.setBounds(118, 12, 65, 21);
        panel.add(radio);
        this.add(panel);
        
        JLabel label = new JLabel("label");
        label.setBounds(117, 11, 61, 21);
        panel.add(label);
        this.add(panel);
        
        JButton tombol = new JButton("Ini Tombol");
        tombol.setBounds(117, 81, 61, 21);
        panel.add(tombol);
        this.add(panel);
    }
    public static void main(String[] args){
     new FrameKU2();   
    }
}