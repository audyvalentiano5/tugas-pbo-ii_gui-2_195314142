package GUI2_3A;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
public class FrameKU3 extends JFrame {
    public FrameKU3(){
        this.setLayout(null);
        this.setSize(300, 160);
        this.setLocation(300, 100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);
        
        JLabel label = new JLabel("Keyword : ");
        label.setBounds(117, 11, 61, 21);
        this.add(label);
        
        JTextField text = new JTextField(null);
        text.setBounds(18, 41, 251, 21);
        this.add(text);
        
        JButton tombol = new JButton("Find");
        tombol.setBounds(117, 81, 61, 21);
        this.add(tombol);
    }
    public static void main(String[] args){
     new FrameKU3();   
    }
}
