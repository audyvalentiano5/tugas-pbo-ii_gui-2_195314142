package Tampilan_GUI_UKM;

import Tampilan_GUI_UKM.Penduduk;
import Tampilan_GUI_UKM.Mahasiswa;
import java.util.ArrayList;

public class UKM {
private String namaUnit;
private Mahasiswa sekretaris;
private Mahasiswa ketua;
private ArrayList<Penduduk> anggota = new ArrayList<Penduduk>();
public UKM(){
}
public UKM(String namaUnit){
    this.namaUnit = namaUnit;
}
public void setNamaUnit(String namaUnit){
    this.namaUnit = namaUnit;
}
public String getNamaUnit(){
    return namaUnit;
}
public void setKetua(Mahasiswa ketua){
    this.ketua = ketua;
}
public Mahasiswa getKetua(){
    return ketua;
}
public void setSekretaris(Mahasiswa sekretaris){
    this.sekretaris = sekretaris;
}
public Mahasiswa getSekretaris(){
    return sekretaris;
}
public ArrayList<Penduduk> getAnggota() {
       return anggota;
   }
public void setAnggota(ArrayList<Penduduk> anggota) {
        this.anggota = anggota;
    }

}
