package Tampilan_GUI_UKM;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import Tampilan_GUI_UKM.mainFrame;
import Tampilan_GUI_UKM.Masyarakat;
import Tampilan_GUI_UKM.Penduduk;
public class masyarakatTable_dialog extends JDialog implements ActionListener {
    private JTable jt;
    private JScrollPane sp;
    private Masyarakat mhs;
    private JLabel label_title;
    private JLabel label_nomor;
    private JLabel label_nama;
    private JLabel label_ttl;
    private JTextField text_nomor;
    private JTextField text_nama;
    private JTextField text_ttl;
    private JTextArea area_nama;
    public masyarakatTable_dialog() {
        init();
    }
    public void init() {
        this.setLayout(null);
        label_title = new JLabel("List Masyarakat");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);
        label_nama = new JLabel("Nama\t :");
        label_nama.setBounds(80, 60, 100, 50);
        this.add(label_nama);
        String asu = "";
        System.out.println(mainFrame.anggota.length);
        for (int i = 0; i < mainFrame.anggota.length; i++) {
            if (mainFrame.anggota[i] != null) {
                asu = asu + " " + mainFrame.anggota[i].getNama();
            }
        }
        area_nama = new JTextArea(asu);
        area_nama.setBounds(180, 75, 180, 20);
        this.add(area_nama);       
        label_nomor = new JLabel("NIM \t :");
        label_nomor.setBounds(80, 100, 100, 50);
        this.add(label_nomor);
        text_nomor = new JTextField(mainFrame.anggota[0].getNama());
        text_nomor.setBounds(180, 115, 180, 20);
        this.add(text_nomor);
        label_ttl = new JLabel("Tempat & tanggal lahir\t :");
        label_ttl.setBounds(80, 140, 100, 50);
        this.add(label_ttl);
        text_ttl = new JTextField(mainFrame.anggota[0].getTTL());
        text_ttl.setBounds(180, 155, 180, 20);
        this.add(text_ttl);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
    }
}
